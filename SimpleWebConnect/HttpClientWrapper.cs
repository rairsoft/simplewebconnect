﻿using System;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace SimpleWebConnect
{
    public class HttpClientWrapper
    {
        public string Post<T>(T t, string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    return Post(client, JsonConvert.SerializeObject(t));
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string Post<T>(T t, string url, string userName, string password)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var cred = Encoding.UTF8.GetBytes($"{userName}:{password}");
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));
                    return Post(client, JsonConvert.SerializeObject(t));
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private string Post(HttpClient client, string json)
        {
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = client.PostAsync("", content).Result;
                return response.StatusCode.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

		public T Get<T>(string url)
		{
			using (var client = new HttpClient ()) 
			{
				var json = Get (client, url);
				return json == null ? default(T) : JsonConvert.DeserializeObject<T> (json);
			}
		}

		private string Get(HttpClient client, string url)
		{
			try
			{
				var response = client.GetAsync(url).Result;
				response.EnsureSuccessStatusCode();
				return response.Content.ReadAsStringAsync().Result;
			}
			catch(Exception ex) 
			{
				Console.WriteLine( ex.ToString());
			}

			return null;
		}
    }
}
