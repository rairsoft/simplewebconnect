# README #

This is an extremely simplified web connect API for use with simple Web APIs.  The goal here isn't to cover all the bases of connecting to the web but rather have a quick and dirty utility for the times when that is needed.

### Contribution guidelines ###

Pull requests welcome.

### Who do I talk to? ###

Join us on the [Hipchat channel](https://rairsoft.hipchat.com/chat/room/2200064)